import { Component, OnInit } from '@angular/core';
import { PaisService } from '../../services/pais.service';

import { Country } from '../../interfaces/pais.interface';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-por-pais',
  templateUrl: './por-pais.component.html',
  styleUrls: ['./por-pais.component.css']
})
export class PorPaisComponent implements OnInit {
  termino:string = '';
  hayError:boolean=false;
  pais$!: Observable<Country[]>;
  paises:Country[]=[];

  paisesSugeridos   : Country[] = [];
  mostrarSugerencias: boolean = false;

  constructor( private paisService:PaisService) { }


  buscar(termino:any){
    this.hayError = false;
    this.termino=termino;
    console.log(this.termino);
    this.paisService.buscarPais(this.termino).subscribe((resp)=>{

      this.paises=resp;
      console.log(this.paises);

    }, (err)=>{
      console.log(err);
      this.hayError = true;
      this.paises=[];
    })

  }


  //buscar2(){
  //this.pais$ = this.paisService.buscarPais(this.termino);
  //console.log(this.pais$);
  //}

  ngOnInit(): void {
  }

  sugerencias( termino: string ) {
    this.hayError = false;
    this.termino = termino;
    this.mostrarSugerencias = true;

    this.paisService.buscarPais( termino )
      .subscribe(
        paises => this.paisesSugeridos = paises.splice(0,5),
        (err) => this.paisesSugeridos = []
      );

  }

  buscarSugerido( termino: string ) {
    this.buscar( termino );
  }

}
