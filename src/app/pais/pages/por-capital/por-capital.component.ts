import { Component, OnInit } from '@angular/core';
import { PaisService } from '../../services/pais.service';

import { Country } from '../../interfaces/pais.interface';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-por-capital',
  templateUrl: './por-capital.component.html',
  styleUrls: ['./por-capital.component.css']
})
export class PorCapitalComponent implements OnInit {

  termino:string = '';
  hayError:boolean=false;
  pais$!: Observable<Country[]>;
  paises:Country[]=[];

  constructor( private paisService:PaisService) { }

  ngOnInit(): void {
  }

  buscar(termino:string){
    this.hayError = false;
    this.termino=termino;
    console.log(this.termino);
    this.paisService.buscarCapital(this.termino).subscribe((resp)=>{

      this.paises=resp;
      console.log(this.paises);

    }, (err)=>{
      console.log(err);
      this.hayError = true;
      this.paises=[];
    })

  }
  sugerencias(termino:string){
    this.hayError = false;
  }

}
